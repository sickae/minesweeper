﻿using System;

namespace MineSweeper
{
    class Scoreboard
    {
        private GameField field;

        public Scoreboard(GameField field) => this.field = field;

        private static object obj = new object();
        public void Display()
        {
            lock (obj)
            {
                ConsoleColor color = Console.ForegroundColor;
                if (field.GameOver)
                    color = field.FreeCells > 0 ? ConsoleColor.Red : ConsoleColor.Green;
                Printer.Print(field.Width + 2, 0,
                    $"Time: " + (field.Time[0] < 10 ? "0" : "") + field.Time[0] + (field.Time[1] < 10 ? ":0" : ":") + field.Time[1], color);
            }
        }
    }
}
