﻿using System;

namespace MineSweeper
{
    class ConsoleDisplay
    {
        private GameField field;

        public ConsoleDisplay(GameField field) => this.field = field;

        private static object obj = new object();
        public void Display()
        {
            lock (obj)
            {
                for (int i = 0; i < field.Width; i++)
                {
                    for (int j = 0; j < field.Height; j++)
                    {
                        Cell c = field.ElementAt(i, j);
                        if (i == field.PlayerX && j == field.PlayerY)
                            Printer.Print(i, j, c.Form, c.Revealed && c.Mine ? ConsoleColor.Red : ConsoleColor.White, c.Flagged ? c.BackgroundColor : ConsoleColor.Gray);
                        else
                            Printer.Print(i, j, c.Form, c.Color, c.BackgroundColor);
                    }
                }
            }
        }
    }
}
