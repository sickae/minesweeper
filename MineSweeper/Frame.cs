﻿using System;
using System.Threading;

namespace MineSweeper
{
    class Frame
    {
        private const int WIDTH = 20;
        private const int HEIGHT = 10;
        GameField field;
        Timer timer;
        ConsoleDisplay cd;
        Scoreboard sb;

        public Frame() => field = new GameField(WIDTH, HEIGHT);

        public void Run()
        {
            cd = new ConsoleDisplay(field);
            sb = new Scoreboard(field);
            timer?.Dispose();
            timer = new Timer(UpdateTime, null, 1000, 1000);
            while(!field.GameOver)
            {
                cd.Display();
                ConsoleKey key = Console.ReadKey(true).Key;
                switch(key)
                {
                    case ConsoleKey.LeftArrow: if (field.PlayerX > 0) field.PlayerX--; break;
                    case ConsoleKey.RightArrow: if (field.PlayerX < WIDTH - 1) field.PlayerX++; break;
                    case ConsoleKey.UpArrow: if (field.PlayerY > 0) field.PlayerY--; break;
                    case ConsoleKey.DownArrow: if (field.PlayerY < HEIGHT - 1) field.PlayerY++; break;
                    case ConsoleKey.Spacebar: field.RevealCell(field.PlayerX, field.PlayerY); break;
                    case ConsoleKey.F: field.Flag(field.PlayerX, field.PlayerY); break;
                }
            }
            cd.Display();
            sb.Display();
            timer.Dispose();
            Thread.Sleep(1000);
            Console.ReadKey();
            Restart();
        }

        private static object obj = new object();
        void UpdateTime(Object state)
        {
            lock (obj)
            {
                field.Tick();
                sb.Display();
            }
        }

        void Restart()
        {
            field = new GameField(WIDTH, HEIGHT);
            Run();
        }
    }
}
