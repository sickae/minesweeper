﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeper
{
    class Cell
    {
        private int x;
        private int y;
        private int near;
        private bool mine;
        private bool revealed;
        private bool flag;
        private GameField field;
        public int X { get => x; }
        public int Y { get => y; }
        public int NearMines { get => near; }
        public bool Mine { get => mine; }
        public bool Revealed { get => revealed; }
        public bool Flagged { get => flag; }
        public char Form { get
            {
                if (!revealed) return '+';
                if (mine) return '*';
                if (near == 0) return ' ';
                return near.ToString()[0];
            } }
        public ConsoleColor Color { get
            {
                if (revealed)
                {
                    if (mine) return ConsoleColor.Black;
                    switch (near)
                    {
                        case 1: return ConsoleColor.Blue;
                        case 2: return ConsoleColor.Green;
                        case 3: return ConsoleColor.Red;
                        case 4: return ConsoleColor.DarkBlue;
                        case 5: return ConsoleColor.DarkRed;
                        case 6: return ConsoleColor.DarkCyan;
                        case 7: return ConsoleColor.Black;
                        case 8: return ConsoleColor.Gray;
                    }
                }
                return ConsoleColor.Gray;
            } }
        public ConsoleColor BackgroundColor { get
            {
                if (flag) return ConsoleColor.DarkCyan;
                return revealed ? ConsoleColor.DarkGray: ConsoleColor.Gray;
            } }

        public Cell(int x, int y, GameField field, bool isMine)
        {
            this.x = x;
            this.y = y;
            this.field = field;
            mine = isMine;
            revealed = false;
            flag = false;
        }

        public void Reveal() => revealed = true;

        public void Flag()
        {
            if (!revealed)
                flag = !flag;
        }

        public void CountNearMines(Cell[] neighbours) => near = neighbours.Where(x => x.Mine).Count();
    }
}
