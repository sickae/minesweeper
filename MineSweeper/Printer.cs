﻿using System;
using System.Text;

namespace MineSweeper
{
    static class Printer
    {

        static Printer()
        {
            Console.CursorVisible = false;
            Console.OutputEncoding = Encoding.Unicode;
        }

        static object obj = new Object();

        public static void Print(int x, int y, char character)
        {
            lock (obj)
            {
                {
                    Console.SetCursorPosition(x, y);
                    Console.Write(character);
                }
            }
        }

        public static void Print(int x, int y, char character, ConsoleColor color)
        {
            lock (obj)
            {
                ConsoleColor temp = Console.ForegroundColor;
                Console.ForegroundColor = color;
                Console.SetCursorPosition(x, y);
                Console.Write(character);
                Console.ForegroundColor = temp;
            }
        }

        public static void Print(int x, int y, char character, ConsoleColor foreground, ConsoleColor background)
        {
            lock (obj)
            {
                ConsoleColor tempf = Console.ForegroundColor;
                ConsoleColor tempb = Console.BackgroundColor;
                Console.ForegroundColor = foreground;
                Console.BackgroundColor = background;
                Console.SetCursorPosition(x, y);
                Console.Write(character);
                Console.ForegroundColor = tempf;
                Console.BackgroundColor = tempb;
            }
        }

        public static void Print(int x, int y, string text)
        {
            for (int i = 0; i < text.Length; i++)
                Print(x + i, y, text[i]);
        }

        public static void Print(int x, int y, string text, ConsoleColor color)
        {
            for (int i = 0; i < text.Length; i++)
                Print(x + i, y, text[i], color);
        }

        public static void Print(int x, int y, string text, ConsoleColor foreground, ConsoleColor background)
        {
            for (int i = 0; i < text.Length; i++)
                Print(x + i, y, text[i], foreground, background);
        }
    }
}
