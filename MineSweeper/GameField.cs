﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MineSweeper
{
    class GameField
    {
        private int width;
        private int height;
        private int playerx;
        private int playery;
        private int freecells;
        private int[] time = new int[2];
        private bool gameover;
        private List<Cell> elements;
        public int Width { get => width; }
        public int Height { get => height; }
        public int PlayerX { get => playerx; set => playerx = value; }
        public int PlayerY { get => playery; set => playery = value; }
        public int FreeCells { get => freecells; set => freecells = value; }
        public int[] Time { get => time; }
        public bool GameOver { get => gameover; set => gameover = value; }

        public GameField(int width, int height)
        {
            this.width = width;
            this.height = height;
            elements = new List<Cell>(width * height);
            gameover = false;
            playerx = width / 2;
            playery = height / 2;
            GenerateMines();
        }

        void GenerateMines()
        {
            // Generating mines
            Random rnd = new Random();
            int mines = (int)(width * height * 0.1);
            freecells = width * height - mines;
            while (elements.Count < mines)
            {
                int x = rnd.Next(0, width);
                int y = rnd.Next(0, height);
                if (elements.Any(e => e.X == x && e.Y == y))
                    continue;
                elements.Add(new Cell(x, y, this, true));
            }
            // Generating rest of the cells
            while (elements.Count < width * height)
            {
                int x = rnd.Next(0, width);
                int y = rnd.Next(0, height);
                if (elements.Any(e => e.X == x && e.Y == y))
                    continue;
                elements.Add(new Cell(x, y, this, false));
            }
            // Calculating neighbouring mines
            foreach(Cell c in elements)
            {
                List<Cell> neighbours = new List<Cell>();
                for (int i = -1; i <= 1; i++)
                    for (int j = -1; j <= 1; j++)
                    {
                        if (i == 0 && j == 0)
                            continue;
                        Cell t = ElementAt(c.X + i, c.Y + j);
                        if (t != null)
                            neighbours.Add(t);
                    }
                c.CountNearMines(neighbours.ToArray());
            }
        }

        public Cell ElementAt(int x, int y) => elements.FirstOrDefault(e => e.X == x && e.Y == y);

        public void RevealCell(int x, int y)
        {
            Cell c = ElementAt(x, y);
            if (c == null || c.Revealed || c.Flagged) return;
            if (c.Mine)
            {
                gameover = true;
                RevealAll();
                return;
            }
            c.Reveal();
            if (--freecells == 0)
                gameover = true;
            if (c.NearMines == 0)
                FloodFill(x, y);
        }

        void FloodFill(int x, int y)
        {
            for (int i = -1; i <= 1; i++)
                for (int j = -1; j <= 1; j++)
                    RevealCell(x + i, y + j);
        }

        void RevealAll()
        {
            foreach (Cell c in elements)
                c.Reveal();
        }

        public void Flag(int x, int y) => ElementAt(x, y)?.Flag();

        public void Tick()
        {
            if(++time[1] == 60)
            {
                time[1] = 0;
                time[0]++;
            }
        }
    }
}
